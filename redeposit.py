import sys
import time
import os
import requests

USERNAME = os.environ["USERNAME"]
PASSWORD = os.environ["PASSWORD"]

LOGIN_URL = "https://doi.crossref.org/servlet/useragent"
SUBMISSION_URL = "https://doi.crossref.org/servlet/submissionAdmin"

class AdminHandler:
    "Automate access to the Admin API."

    def __init__(self):
        self.session = requests.session()

    def login(self):
        print("Logging in...")
        response = self.session.post(LOGIN_URL, {"usr": USERNAME, "pwd": PASSWORD})
        if "Please supply a login and a password" in response.text:
            return False
        else:
            return True

    def submission_can_rerun(self, submission_id, tries=0):
        "Return True if submission can be re-run."

        if tries > 2:
            print("Failed to get submission %d repeatedly, stopping.", submission_id)
            return

        params = {"sf": "detail", "submissionID": submission_id}
        response = self.session.get(SUBMISSION_URL, params=params)
        if "No login info in request" in response.text:
            if self.login():
                return self.submission_can_rerun(submission_id, tries+1)
            else:
                print("Can't log in!")
                return

        if "sf=rerun" in response.text:
            return True
        else:
            return False

    def rerun(self, submission_id):
        response = self.session.get(SUBMISSION_URL, params={"sf": "rerun", "submissionID": submission_id})
        if "The deposit has been submitted" in response.text:
            return True

## Take an input file of submission IDs and re-run each.

handler = AdminHandler()

for line in open(sys.argv[1]):
    submission_id = line.strip()
    can_rerun = handler.submission_can_rerun(submission_id)
    
    if can_rerun:
        rerun = handler.rerun(submission_id)
        print("%s,%r,%r" % (submission_id, can_rerun, rerun))
    else:
        print("%s,%r" % (submission_id, can_rerun))

    time.sleep(0.5)

